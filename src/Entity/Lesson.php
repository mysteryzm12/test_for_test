<?php

namespace App\Entity;

use App\Repository\LessonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LessonRepository::class)
 */
class Lesson
{
    public const LESSON_TYPES = [
        'Прочее',
        'Сложные',
        'Лёгкие',
        'Дипломные',
        'Ознакомительные',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $preview_description;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity=LearningProgram::class, mappedBy="lessons")
     */
    private $learningPrograms;

    public function __construct()
    {
        $this->learningPrograms = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName() ?? '???';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPreviewDescription(): ?string
    {
        return $this->preview_description;
    }

    public function setPreviewDescription(?string $preview_description): self
    {
        $this->preview_description = $preview_description;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeName(): ?string
    {
        return self::LESSON_TYPES[$this->getType()] ?? null;
    }

    /**
     * @return Collection|LearningProgram[]
     */
    public function getLearningPrograms(): Collection
    {
        return $this->learningPrograms;
    }

    public function addLearningProgram(LearningProgram $learningProgram): self
    {
        if (!$this->learningPrograms->contains($learningProgram)) {
            $this->learningPrograms[] = $learningProgram;
            $learningProgram->addLesson($this);
        }

        return $this;
    }

    public function removeLearningProgram(LearningProgram $learningProgram): self
    {
        if ($this->learningPrograms->removeElement($learningProgram)) {
            $learningProgram->removeLesson($this);
        }

        return $this;
    }
}
