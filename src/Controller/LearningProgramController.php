<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LearningProgramController extends AbstractController
{
    /**
     * @Route("/learning_program", name="app_learning_program")
     */
    public function index(): Response
    {
        return $this->render('learning_program/index.html.twig', [
            'controller_name' => 'LearningProgramController',
        ]);
    }
}
