<?php


namespace App\Admin;

use App\Entity\Lesson;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class LessonAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('name', TextType::class)
            ->add('previewDescription', TextareaType::class)
            ->add('description', TextareaType::class)
            ->add('type', ChoiceType::class, [
                'multiple' => false,
                'choices' => array_flip(Lesson::LESSON_TYPES)
            ])
        ;
    }
    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('name')
            ->add('previewDescription')
            ->add('description')
            ->add('typeName')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('name')
            ->add('previewDescription')
        ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->addIdentifier('id')
            ->add('name')
            ->add('previewDescription')
        ;
    }
}
