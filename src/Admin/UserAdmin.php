<?php


namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserAdmin extends AbstractAdmin
{
    private UserPasswordHasherInterface $passwordEncoder;

    public function injectDependencies(UserPasswordHasherInterface $passwordEncoder): void
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('email', TextType::class)
            ->add('password', PasswordType::class)
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'required' => false,
                'choices' => [
                    'Админ' => 'ROLE_ADMIN',
                    'Просто роль' => 'ROLE_USER_1',
                    'Просто роль 2' => 'ROLE_USER_2',
                    'Соната админ' => 'ROLE_SONATA_ADMIN',
                ]
            ])
        ;
    }

    protected function prePersist(object $object): void
    {
        /** @var User $object */
        $object->setPassword($this->passwordEncoder->hashPassword($object, $object->getPassword()));
    }

    protected function preUpdate(object $object): void
    {
        /** @var User $object */
        $object->setPassword($this->passwordEncoder->hashPassword($object, $object->getPassword()));
    }

    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('email')
            ->add('roles')
        ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->addIdentifier('id')
            ->add('email')
            ->add('roles')
        ;
    }
}
